package com.hendisantika.bazservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@SpringBootApplication
public class BazServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BazServiceApplication.class, args);
    }
}

@RestController
class BazController {

    @GetMapping("/")
    String bar() {
        return "Welcome to bar service! " + new Date();
    }
}